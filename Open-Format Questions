**Open-Format Questions:**

1. Scalar Data Types:
**Open Format:** Explain the purpose of the BOOLEAN data type in PL/SQL and provide a practical example of how it can be used in a conditional statement.

Answer: Pl/SQL-dəki BOOLEAN data type-ı məntiqi dəyərləri özündə daşıyaraq, DOĞRU və ya YANLIŞ ola bilir. Bu data type adətən hər hansısa bir şərt qoyulduqda istifadə edilir.
Example: DECLARE
  v_weekend BOOLEAN := TRUE;
BEGIN
  IF v_weekend THEN
    DBMS_OUTPUT.PUT_LINE('Bugun heftesonudur.');
  ELSE
    DBMS_OUTPUT.PUT_LINE('Bugun is gunudur.');
  END IF;
END;


2. Composite Data Types:
**Open Format:** Create a Nested Table in PL/SQL named `employee_skills` to store the skills of five employees. Populate the Nested Table with employee names and their respective skills. Display the contents of the Nested Table.

Answer: DECLARE

  TYPE list_of_skills IS TABLE OF VARCHAR2(100);

  employee_skills list_of_skills := list_of_skills();

BEGIN

  employee_skills.extend(5);
  employee_skills(1) := 'Neena Kochhar, Programming, Database Management';
  employee_skills(2) := 'Bruce Ernst, Marketing, Leadership';
  employee_skills(3) := 'David Auston, Data Analysis, Oracle SQL';
  employee_skills(4) := 'Nancy Brown, Communication, Speaking';
  employee_skills(5) := 'Robert Lee, Networking, Security';


  DBMS_OUTPUT.PUT_LINE('Employee 1: ' || employee_skills(1));
  DBMS_OUTPUT.PUT_LINE('Employee 2: ' || employee_skills(2));
  DBMS_OUTPUT.PUT_LINE('Employee 3: ' || employee_skills(3));
  DBMS_OUTPUT.PUT_LINE('Employee 4: ' || employee_skills(4));
  DBMS_OUTPUT.PUT_LINE('Employee 5: ' || employee_skills(5));
END;

3. Reference Data Types:
**Open Format:** Describe the differences between the REF CURSOR and the REF data types in PL/SQL and provide specific use cases where each type would be more suitable.

Answer: REF CURSOR data type-ı bizə kursora istinad yaratmaq imkanı verir. Bu adətən PL/SQL-də saxlanılan prosedur və funksiyalardan ibarət sorğu nəticələri qaytarmaq üçün istifadə olunur. Use case: Returning Query Results, Dynamic Queries.
REF data type-ı verilənlər bazasında obyektlərə və sətirlərə istinad verə bilmək üçündür. Bu, mövcud obyektlər üçün göstəricilər yaratmağa və onlardan PL/SQL kodu daxilində istifadə etməyə imkan verir. Use case: Object-Oriented Programming (OOP), Parent-Child Relationships.

4. LOB Data Types:
**Open Format:** Explain the purpose of the CLOB data type in PL/SQL and provide a real-world use case where it would be beneficial.

Answer: PL/SQL-də CLOB (Character Large Object) data type-nın əsas məqsədi VARCHAR2 məlumat növünün tutumunu aşan mətn, sənədlər və ya böyük həcmli mətn məlumatlarını saxlamaqdır. CLOB məlumat növü bizə 4 GB qədər mətn məlumatını saxlamağa imkan verir. Real-World Use Case: CLOB məlumat növünün ən çox istifadə ediləcəyi və faydalı olacağı başlıca hallardan biri blog platformasını deyə bilərik. Hansı ki, burada istifadəçilər məqalələr yaza, dərc edə bilə və bu məqalələrə şəkillər və digər multimedia elementləri əlavə edə bilərlər. Bu da kifayət qədər irihəcmli mətn tipli informasiya deməkdir. Belə olduğu halda biz CLOB müraciət edəcəyik.

5. Collection Data Types:
**Open Format:** Compare and contrast VARRAY, Nested Table, and Associative Array (PL/SQL Table) in terms of their usage, limitations, and scenarios where each would be more appropriate.

Answer: VARRAY - həm də massivdir (array-dir). Lakin VARRAY-lərin təyini zamanı müəyyən edilmiş sabit max həddi var. Bir VARRAY yaradıldıqdan sonra onun ölçüsü dəyişə bilməz.

Nested Table - ehtiyac olduqda böyüyə və ya kiçilə bilən dinamik ölçülü kolleksiyaya ehtiyacımız olan hallar üçün uyğundur.Bu nlar iş zamanı elementlərin səmərəli daxil edilməsinə və silinməsinə imkan verir.

Associative Array - Associative array nested table-da və VARRAY-larda olduğu kimi ardıcıl tam ədədlərlə deyil, unikal açarla indeksləşdirilmiş elementlər toplusudur. Bu, digər proqramlaşdırma dillərində lüğət və ya hash cədvəlinə bənzəyir.
